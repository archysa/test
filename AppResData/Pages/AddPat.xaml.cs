﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp4.AppResData.DB;

namespace WpfApp4.AppResData.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddPat.xaml
    /// </summary>
    public partial class AddPat : Page
    {
        private pat _currentPat = new pat();
        public AddPat()
        {
            InitializeComponent();
            DataContext = _currentPat;
        }

        private void AddPat_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder errors = new StringBuilder();

            


            if (_currentPat.id == 0)
                Entitites.GetContext().pat.Add(_currentPat);

            try 
            {
                Entitites.GetContext().SaveChanges();
                MessageBox.Show("Успешно");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }
    }
}
