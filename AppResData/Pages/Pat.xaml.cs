﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp4.AppResData.Classes;
using WpfApp4.AppResData.DB;

namespace WpfApp4.AppResData.Pages
{
    /// <summary>
    /// Логика взаимодействия для Pat.xaml
    /// </summary>
    public partial class Pat : Page
    {


        public Pat()
        {
            InitializeComponent();
            PatDGrid.ItemsSource = Entitites.GetContext().pat.ToList();

        }

        private void AddPat_Click(object sender, RoutedEventArgs e)
        {
            Navigator.MainFrame.Navigate(new AddPat());
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Visibility == Visibility.Visible)
            {
                Entitites.GetContext().ChangeTracker.Entries().ToList().ForEach(p => p.Reload());
                PatDGrid.ItemsSource = Entitites.GetContext().pat.ToList();
            }
        }
    }
}
